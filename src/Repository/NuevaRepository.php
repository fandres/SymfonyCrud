<?php

namespace App\Repository;

use App\Entity\Nueva;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Nueva|null find($id, $lockMode = null, $lockVersion = null)
 * @method Nueva|null findOneBy(array $criteria, array $orderBy = null)
 * @method Nueva[]    findAll()
 * @method Nueva[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class NuevaRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Nueva::class);
    }

//    /**
//     * @return Nueva[] Returns an array of Nueva objects
//     */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('n')
            ->andWhere('n.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('n.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Nueva
    {
        return $this->createQueryBuilder('n')
            ->andWhere('n.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
