<?php declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20180802060952 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE entrenador (id INT AUTO_INCREMENT NOT NULL, nombre VARCHAR(255) NOT NULL, apellido VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE entrenador_equipo (entrenador_id INT NOT NULL, equipo_id INT NOT NULL, INDEX IDX_86C371F34FE90CDB (entrenador_id), INDEX IDX_86C371F323BFBED (equipo_id), PRIMARY KEY(entrenador_id, equipo_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE equipo (id INT AUTO_INCREMENT NOT NULL, nombre VARCHAR(255) NOT NULL, categoria VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE entrenador_equipo ADD CONSTRAINT FK_86C371F34FE90CDB FOREIGN KEY (entrenador_id) REFERENCES entrenador (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE entrenador_equipo ADD CONSTRAINT FK_86C371F323BFBED FOREIGN KEY (equipo_id) REFERENCES equipo (id) ON DELETE CASCADE');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE entrenador_equipo DROP FOREIGN KEY FK_86C371F34FE90CDB');
        $this->addSql('ALTER TABLE entrenador_equipo DROP FOREIGN KEY FK_86C371F323BFBED');
        $this->addSql('DROP TABLE entrenador');
        $this->addSql('DROP TABLE entrenador_equipo');
        $this->addSql('DROP TABLE equipo');
    }
}
