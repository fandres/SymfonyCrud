<?php

namespace App\Controller;

use App\Entity\Nueva;
use App\Form\NuevaType;
use App\Repository\NuevaRepository;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/nueva")
 */
class NuevaController extends Controller
{
    /**
     * @Route("/", name="nueva_index", methods="GET")
     */
    public function index(NuevaRepository $nuevaRepository): Response
    {
        return $this->render('nueva/index.html.twig', ['nuevas' => $nuevaRepository->findAll()]);
    }

    /**
     * @Route("/new", name="nueva_new", methods="GET|POST")
     */
    public function new(Request $request): Response
    {
        $nueva = new Nueva();
        $form = $this->createForm(NuevaType::class, $nueva);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($nueva);
            $em->flush();

            return $this->redirectToRoute('nueva_index');
        }

        return $this->render('nueva/new.html.twig', [
            'nueva' => $nueva,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="nueva_show", methods="GET")
     */
    public function show(Nueva $nueva): Response
    {
        return $this->render('nueva/show.html.twig', ['nueva' => $nueva]);
    }

    /**
     * @Route("/{id}/edit", name="nueva_edit", methods="GET|POST")
     */
    public function edit(Request $request, Nueva $nueva): Response
    {
        $form = $this->createForm(NuevaType::class, $nueva);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('nueva_edit', ['id' => $nueva->getId()]);
        }

        return $this->render('nueva/edit.html.twig', [
            'nueva' => $nueva,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="nueva_delete", methods="DELETE")
     */
    public function delete(Request $request, Nueva $nueva): Response
    {
        if ($this->isCsrfTokenValid('delete'.$nueva->getId(), $request->request->get('_token'))) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($nueva);
            $em->flush();
        }

        return $this->redirectToRoute('nueva_index');
    }
}
